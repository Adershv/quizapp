package com.example.user.quizapp.model.Quiz;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.user.quizapp.classes.VolleySingleton;
import com.example.user.quizapp.model.Constants;
import com.example.user.quizapp.presenter.quiz.QuizPresenter;
import com.example.user.quizapp.view.quiz.QuizView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class QuizModel implements QuizPresenter {

    QuizView quizView;
    Context context;
    SharedPreferences sharedPreferences;

    String quizType,questionUrl;
    int quizCategory;

    Question  question;
    List<Question> questionList= new ArrayList<Question>();


    public QuizModel(QuizView quizView) {
        this.quizView = quizView;

    }

    @Override
    public void loadQuestions(final Context context) {
        sharedPreferences=context.getSharedPreferences(Constants.QUIZ_INFO,Context.MODE_PRIVATE);
        quizCategory=sharedPreferences.getInt(Constants.QUIZ_CATEGORY,0);
        quizType=sharedPreferences.getString(Constants.QUIZ_TYPE,"null");
        questionUrl="https://opentdb.com/api.php?amount=5&category="+quizCategory+"&difficulty="+quizType+"&type=multiple&encode=url3986";

        StringRequest stringRequest=new StringRequest(Request.Method.GET, questionUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray=jsonObject.getJSONArray("results");

                    for(int i=0; i<jsonArray.length();i++){
                        JSONObject obj=jsonArray.getJSONObject(i);
                        question=new Question();

                        question.question=java.net.URLDecoder.decode(obj.getString("question"), String.valueOf(StandardCharsets.UTF_8));
                        question.answer=java.net.URLDecoder.decode(obj.getString("correct_answer"), String.valueOf(StandardCharsets.UTF_8));
                        JSONArray incorrectAns=obj.getJSONArray("incorrect_answers");
                        question.op1=java.net.URLDecoder.decode(incorrectAns.getString(0), String.valueOf(StandardCharsets.UTF_8));
                        question.op2=java.net.URLDecoder.decode(incorrectAns.getString(1), String.valueOf(StandardCharsets.UTF_8));
                        question.op3=java.net.URLDecoder.decode(incorrectAns.getString(2), String.valueOf(StandardCharsets.UTF_8));
                        questionList.add(question);

                    }





                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }


                quizView.onQuestionLoadFinish(questionList);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context,error.toString(),Toast.LENGTH_SHORT).show();
            }
        });

        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);



    }


}
