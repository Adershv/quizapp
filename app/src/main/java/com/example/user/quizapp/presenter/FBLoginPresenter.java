package com.example.user.quizapp.presenter;

import android.content.Intent;

public interface FBLoginPresenter {
    void fbLoginInit();
    void login();
    void onActivityResult(int requestCode, int resultCode, Intent data);
    void logout();
}
