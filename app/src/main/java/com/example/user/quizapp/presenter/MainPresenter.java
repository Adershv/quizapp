package com.example.user.quizapp.presenter;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.view.View;
import android.widget.ScrollView;

import com.example.user.quizapp.model.QuizCategory;

import java.util.List;

public interface MainPresenter {
    void getQuizCategory(Context context);

    interface DynamicCardview{
        void createCardView(ScrollView scrollView, List<QuizCategory> quizCategories, Context context);
    }
    interface CustomDialog{
        void createDialog(Context context, View view);
    }
}
