package com.example.user.quizapp.model;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.example.user.quizapp.activity.LoginActivity;
import com.example.user.quizapp.activity.MainActivity;
import com.example.user.quizapp.presenter.LoginPresenter;
import com.example.user.quizapp.view.LoginView;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;

import static com.facebook.FacebookSdk.getApplicationContext;

public class LoginModel implements LoginPresenter {

    int RC_SIGN_IN=100;
    GoogleSignInClient mGoogleSignInClient;

    LoginView loginView;
    Context LoginContext;
    UserSingleton userSingleton=UserSingleton.getInstance();

    private static final String TAG = "LoginModel";


    public LoginModel(LoginView loginView,Context context) {
        this.loginView=loginView;
        this.LoginContext=context;

    }


    @Override
    public void LoginWithGoogle(LoginActivity loginActivity) {
        loginView.showProgressBar();
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        loginActivity.startActivityForResult(signInIntent, RC_SIGN_IN);

    }

    @Override
    public void setUpGoogleClient(Context context) {
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(context, gso);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    @Override
    public void LoginWithFacebook() {

    }

    @Override
    public void LoginWithLinkedin() {

    }

    @Override
    public boolean isLogined(LoginActivity loginActivity) {
        // Check for existing Google Sign In account, if the user is already signed in
        // the GoogleSignInAccount will be non-null.
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(loginActivity);
        if(account!=null){
            return true;
        }
        return false;
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            //updateUI(account);

            userSingleton.setService(Constants.GOOGLE_SERVICE);
            userSingleton.setLoginStatus(Constants.LOGIN_IN);
            userSingleton.setUserName(account.getDisplayName());
            //userSingleton.readUserData(getApplicationContext());
            //Toast.makeText(getApplicationContext(),account.getDisplayName()+"test",Toast.LENGTH_SHORT).show();
            userSingleton.setEmail(account.getEmail());
            userSingleton.saveUserData(getApplicationContext());
            loginView.hideProgressBar();
            loginView.onLoginSuccess();
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
            loginView.onLoginFailed();
        }
    }

    @Override
    public void logout(LoginActivity loginActivity) {

        mGoogleSignInClient.signOut()
                .addOnCompleteListener(loginActivity, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        userSingleton.setLoginStatus(Constants.LOGIN_OUT);
                        userSingleton.setService(Constants.GOOGLE_SERVICE);
                        userSingleton.saveUserData(getApplicationContext());
                        loginView.onLogout();
                    }
                }).addOnFailureListener(loginActivity, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                        loginView.onLogoutFailed();
            }
        });

    }
}
