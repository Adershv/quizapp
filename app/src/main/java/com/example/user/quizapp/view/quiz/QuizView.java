package com.example.user.quizapp.view.quiz;

import com.example.user.quizapp.model.Quiz.Question;

import java.util.List;

public interface QuizView  {
    void onQuestionLoadFinish(List<Question> questions);

    interface QuizManager{
        void nextQuestion(Question question);
        void updateTimer(String s);
        void onQuizEnd();

    }
}
