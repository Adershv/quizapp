package com.example.user.quizapp.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.user.quizapp.R;
import com.example.user.quizapp.model.Constants;
import com.example.user.quizapp.model.FBLoginModel;
import com.example.user.quizapp.model.LoginModel;
import com.example.user.quizapp.model.UserSingleton;
import com.example.user.quizapp.presenter.FBLoginPresenter;
import com.example.user.quizapp.presenter.LoginPresenter;
import com.example.user.quizapp.view.LoginView;
import com.facebook.FacebookSdk;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.SignInButton;

public class LoginActivity extends AppCompatActivity implements LoginView {

    LoginPresenter loginPresenter;
    FBLoginPresenter fbLoginPresenter;

    SignInButton signInButton;
    LoginButton loginButtonfb;
    ProgressBar progressBar;

    UserSingleton  userSingleton=UserSingleton.getInstance();

    BroadcastReceiver broadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        userSingleton.readUserData(getApplicationContext());
        if (userSingleton.getLoginStatus()==1){
            Intent intent =new Intent(this,MainActivity.class);
            startActivity(intent);
            //finish();

        }

        setContentView(R.layout.activity_login);

        init();
        loginPresenter.setUpGoogleClient(this);
        fbLoginPresenter.fbLoginInit();







        broadcastReceiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if("com.example.user.quizapp.LOGOUT".equals(intent.getAction())){
                    if(intent.getStringExtra("com.example.user.quizapp.EXTRA_VALUE").equals("logoutfb")){

                        fbLoginPresenter.logout();
                    }
                    if(intent.getStringExtra("com.example.user.quizapp.EXTRA_VALUE").equals("logoutgoogle")){

                        loginPresenter.logout(LoginActivity.this);
                    }

                }


            }
        };

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginPresenter.LoginWithGoogle(LoginActivity.this);
            }
        });
        loginButtonfb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fbLoginPresenter.login();
            }
        });


    }

    @Override
    protected void onStart() {
        IntentFilter filter=new IntentFilter("com.example.user.quizapp.LOGOUT");
        registerReceiver(broadcastReceiver,filter);
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        if(broadcastReceiver!=null)
        unregisterReceiver(broadcastReceiver);
        super.onDestroy();
    }

    private void init() {

        progressBar=findViewById(R.id.progressBar);
        signInButton = findViewById(R.id.sign_in_button);
        loginButtonfb=findViewById(R.id.login_button);
        loginButtonfb.setReadPermissions("email", "public_profile","user_birthday", "user_photos");

        fbLoginPresenter=new FBLoginModel(this,this);
        loginPresenter=new LoginModel(this,this);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==100){
            loginPresenter.onActivityResult(requestCode,resultCode,data);
        }
        else {
            fbLoginPresenter.onActivityResult(requestCode,resultCode,data);
        }

    }

    @Override
    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onLoginSuccess() {

        Intent loginIntent =new Intent(LoginActivity.this,MainActivity.class);
        startActivity(loginIntent);


    }

    @Override
    public void onLoginFailed() {
        Toast.makeText(this,"Login Failed",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLogout() {


        userSingleton.readUserData(getApplicationContext());

        if(userSingleton.getService()== Constants.FB_SERVICE){


            Intent intent =new Intent("com.example.user.quizapp.LOGOUTSUCCESS");
            intent.putExtra("com.example.user.quizapp.EXTRA_VALUE","LOGOUTSUCCESSFB");
            sendBroadcast(intent);
        }
        if (userSingleton.getService() == Constants.GOOGLE_SERVICE){

            Intent intent =new Intent("com.example.user.quizapp.LOGOUTSUCCESS");
            intent.putExtra("com.example.user.quizapp.EXTRA_VALUE","LOGOUTSUCCESSGOOGLE");
            sendBroadcast(intent);
        }


    }

    @Override
    public void onLogoutFailed() {
        Toast.makeText(this,"Logout Failed",Toast.LENGTH_SHORT).show();
    }
}
