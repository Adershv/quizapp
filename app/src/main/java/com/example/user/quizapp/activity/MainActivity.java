package com.example.user.quizapp.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.example.user.quizapp.R;
import com.example.user.quizapp.model.Constants;
import com.example.user.quizapp.model.MainModel;
import com.example.user.quizapp.model.QuizCategory;
import com.example.user.quizapp.model.UserSingleton;
import com.example.user.quizapp.presenter.DynamicCardview;
import com.example.user.quizapp.presenter.MainPresenter;
import com.example.user.quizapp.view.MainView;

import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, MainView,MainView.DynamicCardView ,MainView.CustomDialog{

    ScrollView scrollView;
    BroadcastReceiver logoutBroadcast;

    UserSingleton userSingleton;
    MainPresenter mainPresenter;
    MainPresenter.DynamicCardview dynamicCardview;
    MainPresenter.CustomDialog customDialog;
    LinearLayout linearLayout;


    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        init();
        sharedPreferences=getApplicationContext().getSharedPreferences(Constants.QUIZ_INFO,Context.MODE_PRIVATE);
        editor=sharedPreferences.edit();
        mainPresenter =new MainModel(this);
        dynamicCardview=new DynamicCardview(this);
        mainPresenter.getQuizCategory(this);
        customDialog=new com.example.user.quizapp.presenter.CustomDialog(this);



        userSingleton=UserSingleton.getInstance();
        userSingleton.readUserData(getApplicationContext());
        String username=userSingleton.getUserName();
        Toast.makeText(MainActivity.this,"Welcome "+username,Toast.LENGTH_SHORT).show();

        logoutBroadcast=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if("com.example.user.quizapp.LOGOUTSUCCESS".equals(intent.getAction())){
                    if (intent.getStringExtra("com.example.user.quizapp.EXTRA_VALUE").equals("LOGOUTSUCCESSFB")){
                        userSingleton.setService(Constants.FB_SERVICE);
                        userSingleton.setLoginStatus(Constants.LOGIN_OUT);
                        userSingleton.saveUserData(getApplicationContext());
                        Intent lououtIntent =new Intent(MainActivity.this,LoginActivity.class);
                        startActivity(lououtIntent);
                        finish();
                    }
                    if(intent.getStringExtra("com.example.user.quizapp.EXTRA_VALUE").equals("LOGOUTSUCCESSGOOGLE")){


                        userSingleton.setService(Constants.FB_SERVICE);
                        userSingleton.setLoginStatus(Constants.LOGIN_OUT);
                        userSingleton.saveUserData(getApplicationContext());
                        Intent lououtIntent =new Intent(MainActivity.this,LoginActivity.class);
                        startActivity(lououtIntent);
                        finish();
                    }

                }

            }
        };




    }

    private void init() {


        scrollView=findViewById(R.id.scrollview);
        linearLayout=scrollView.findViewById(R.id.item_container);



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_activity_actionbar_menu,menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                logout();
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }
    @Override
    public void onClick(View view) {

            View dialogView=getLayoutInflater().inflate(R.layout.custom_dialog_layout,null);
            //editor=getSharedPreferences(Constants.QUIZ_INFO,Context.MODE_PRIVATE).edit();
            editor.putInt(Constants.QUIZ_CATEGORY,view.getId());
            editor.commit();
            customDialog.createDialog(this,dialogView);


    }

    private void logout(){
        userSingleton.readUserData(getApplicationContext());

        if(userSingleton.getService()== Constants.FB_SERVICE){

            Intent intent=new Intent("com.example.user.quizapp.LOGOUT");
            intent.putExtra("com.example.user.quizapp.EXTRA_VALUE","logoutfb");
            sendBroadcast(intent);
        }
        if(userSingleton.getService()== Constants.GOOGLE_SERVICE) {

            Intent intent = new Intent("com.example.user.quizapp.LOGOUT");
            intent.putExtra("com.example.user.quizapp.EXTRA_VALUE", "logoutgoogle");
            sendBroadcast(intent);
        }

    }
    @Override
    protected void onStart() {

        IntentFilter filter=new IntentFilter("com.example.user.quizapp.LOGOUTSUCCESS");
        registerReceiver(logoutBroadcast,filter);
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(logoutBroadcast);
        super.onDestroy();
    }

    @Override
    public void showProgessBar() {

    }

    @Override
    public void hideProgressBar() {

    }
    @Override
    public void onSelectDialogOption(String selectedQuizType) {
        editor.putString(Constants.QUIZ_TYPE,selectedQuizType);
        editor.commit();
        Intent intent=new Intent(MainActivity.this,QuizActivity.class);
        startActivity(intent);

    }

    @Override
    public void afterLoadQuizCategory(List<QuizCategory> quizCategories) {

        if (quizCategories.size() > 1) {

            dynamicCardview.createCardView(scrollView, quizCategories, this);

        }
    }

    @Override
    public void onCardViewCreateFinish() {

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            Log.d("CDA", "onKeyDown Called");
            onBackPressed();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }


    @Override
    public void onBackPressed() {
        Log.d("CDA", "onBackPressed Called");
        Intent setIntent = new Intent(Intent.ACTION_MAIN);
        setIntent.addCategory(Intent.CATEGORY_HOME);
        setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(setIntent);
        finish();
    }
}
