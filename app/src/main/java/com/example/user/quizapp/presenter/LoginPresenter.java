package com.example.user.quizapp.presenter;

import android.content.Context;
import android.content.Intent;

import com.example.user.quizapp.activity.LoginActivity;
import com.example.user.quizapp.activity.MainActivity;
import com.example.user.quizapp.view.LoginView;

public interface LoginPresenter {
    void LoginWithGoogle(LoginActivity loginActivity);
    void setUpGoogleClient(Context context);
    void onActivityResult(int requestCode, int resultCode, Intent data);
    void LoginWithFacebook();
    void LoginWithLinkedin();
    boolean isLogined(LoginActivity loginActivity);
    void logout(LoginActivity loginActivity);//Logout from app and return true if success else it will return false

}
