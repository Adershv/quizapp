package com.example.user.quizapp.model;

import android.content.Context;
import android.content.SharedPreferences;

public class UserSingleton {

    private String userName;
    private String email;
    private int loginStatus;
    private int service;
    SharedPreferences readUserData;
    SharedPreferences.Editor writeUserData;

    private static UserSingleton userSingleton;

    public static UserSingleton getInstance(){
        if(userSingleton==null){
            userSingleton=new UserSingleton();
            return userSingleton;
        }
        return userSingleton;
    }

    public void saveUserData(Context context){
        writeUserData=context.getSharedPreferences(Constants.USER_INFO,Context.MODE_PRIVATE).edit();
        writeUserData.putString(Constants.USER_NAME,userName);
        writeUserData.putString(Constants.USER_EMAIL,email);
        writeUserData.putInt(Constants.SERVICE,service);
        writeUserData.putInt(Constants.LOGIN_STATUS,loginStatus);
        writeUserData.commit();
    }

    public void readUserData(Context context){
        readUserData=context.getSharedPreferences(Constants.USER_INFO,Context.MODE_PRIVATE);
        userName=readUserData.getString(Constants.USER_NAME,"");
        email=readUserData.getString(Constants.USER_EMAIL,"");
        service=readUserData.getInt(Constants.SERVICE,-1);
        loginStatus=readUserData.getInt(Constants.LOGIN_STATUS,-1);
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getLoginStatus() {
        return loginStatus;
    }

    public void setLoginStatus(int loginStatus) {
        this.loginStatus = loginStatus;
    }

    public int getService() {
        return service;
    }

    public void setService(int service) {
        this.service = service;
    }
}
