package com.example.user.quizapp.view;

import com.example.user.quizapp.model.QuizCategory;

import java.util.List;

public interface MainView {

    void showProgessBar();
    void hideProgressBar();
    void afterLoadQuizCategory(List<QuizCategory> quizCategories);

    interface DynamicCardView{
        void onCardViewCreateFinish();

    }
    interface CustomDialog{
        void onSelectDialogOption(String selectedQuizType);
    }
}
