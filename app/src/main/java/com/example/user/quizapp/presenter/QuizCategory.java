package com.example.user.quizapp.presenter;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.example.user.quizapp.R;
import com.example.user.quizapp.view.MainView;

import java.util.List;

public class QuizCategory implements MainPresenter.DynamicCardview {
    MainView.DynamicCardView dynamicCardView;

    public QuizCategory(MainView.DynamicCardView dynamicCardView) {
        this.dynamicCardView = dynamicCardView;
    }

    @Override
    public void createCardView(ScrollView scrollView, List<com.example.user.quizapp.model.QuizCategory> quizCategories, Context context) {
        LinearLayout  linearLayout=scrollView.findViewById(R.id.item_container);
        LinearLayout.LayoutParams layoutParams=new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        for (int i=0;i<quizCategories.size();i++){
            Button button=new Button(context);
            button.setLayoutParams(layoutParams);
            button.setText(quizCategories.get(i).getCat_name());
            linearLayout.addView(button);

        }
    }
}
