package com.example.user.quizapp.view;

public interface LoginView {
    void showProgressBar();
    void hideProgressBar();
    void onLoginSuccess();
    void onLoginFailed();
    void onLogout();
    void onLogoutFailed();

}

