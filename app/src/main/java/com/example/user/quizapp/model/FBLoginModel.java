package com.example.user.quizapp.model;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.example.user.quizapp.presenter.FBLoginPresenter;
import com.example.user.quizapp.view.LoginView;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;

import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

import static com.facebook.FacebookSdk.getApplicationContext;

public class FBLoginModel implements FBLoginPresenter {


    LoginView loginView;
    Context context;
    CallbackManager callbackManager;
    UserSingleton userSingleton=UserSingleton.getInstance();
    private List<String> permissionNeeds = Arrays.asList("public_profile", "email",  "user_photos", "user_birthday");
    private static final int RC_SIGN_IN_FB = 1916;
    public FBLoginModel(LoginView loginView,Context context) {
        this.loginView=loginView;
        this.context=context;
    }

    @Override
    public void fbLoginInit() {

        FacebookSdk.sdkInitialize(context);

    }

    @Override
    public void login() {
        loginView.showProgressBar();
        callbackManager=CallbackManager.Factory.create();
        FacebookCallback<LoginResult> callback=new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {


                            userSingleton.setService(Constants.FB_SERVICE);
                            userSingleton.setLoginStatus(Constants.LOGIN_IN);
                            userSingleton.saveUserData(getApplicationContext());
                            loginView.hideProgressBar();
                            loginView.onLoginSuccess();
                            accessTokenTracker.startTracking();

            }

            @Override
            public void onCancel() {
                loginView.onLoginFailed();

            }

            @Override
            public void onError(FacebookException error) {

            }
        };
        LoginManager.getInstance().logInWithReadPermissions((Activity)loginView,permissionNeeds);
        LoginManager.getInstance().registerCallback(callbackManager,callback);

    }

    AccessTokenTracker accessTokenTracker=new AccessTokenTracker() {
        @Override
        protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {

            if(currentAccessToken!=null){
                loadUserData(currentAccessToken);
            }

        }
    };

    private void loadUserData(AccessToken accessToken){
        GraphRequest request= GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {

                try{

                    String username=object.getString("first_name");
                    userSingleton.setUserName(username);
                    //userSingleton.setEmail(object.getString("email"));
                    userSingleton.saveUserData(getApplicationContext());
                    userSingleton.readUserData(getApplicationContext());

                }
                catch (Exception e){
                    Toast.makeText(context, e.toString(), Toast.LENGTH_LONG).show();
                }
            }
        });
        Bundle bundle=new Bundle();
        bundle.putString("fields","first_name,last_name,email");
        request.setParameters(bundle);
        request.executeAsync();
    }

    public boolean isLoggedIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null;
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


            callbackManager.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public void logout() {

        LoginManager.getInstance().logOut();
        loginView.onLogout();


    }
}
