package com.example.user.quizapp.presenter;

import android.content.Context;
import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.CardView;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.example.user.quizapp.R;
import com.example.user.quizapp.model.QuizCategory;
import com.example.user.quizapp.view.LoginView;
import com.example.user.quizapp.view.MainView;

import java.util.List;

import static com.facebook.FacebookSdk.getApplicationContext;

public class DynamicCardview implements MainPresenter.DynamicCardview {

    MainView.DynamicCardView dynamicCardView;
    TextView textView;
    LinearLayout.LayoutParams layoutParams;

    public DynamicCardview(MainView.DynamicCardView dynamicCardView) {
        this.dynamicCardView = dynamicCardView;
    }


    @Override
    public void createCardView(ScrollView scrollView, List<QuizCategory> quizCategories, Context context) {

        LinearLayout linearLayout=scrollView.findViewById(R.id.item_container);

        for(int i=0;i<quizCategories.size();i++){
            layoutParams=new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.topMargin=10;
            Button button=new Button(context);
            button.setId(quizCategories.get(i).getCat_id());
            button.setText(quizCategories.get(i).getCat_name());
            button.setLayoutParams(layoutParams);

            button.setOnClickListener((View.OnClickListener) context);
            linearLayout.addView(button);
        }

    }
}
