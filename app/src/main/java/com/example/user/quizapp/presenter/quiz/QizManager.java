package com.example.user.quizapp.presenter.quiz;

import android.app.AlertDialog;
import android.content.Context;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.user.quizapp.R;
import com.example.user.quizapp.model.Constants;
import com.example.user.quizapp.model.Quiz.Question;
import com.example.user.quizapp.view.quiz.QuizView;

import java.util.List;

public class QizManager implements QuizPresenter.QuizManger {

    QuizView.QuizManager quizManagerView;
    CountDownTimer countDownTimer;

    public QizManager(QuizView.QuizManager quizManager) {
        this.quizManagerView = quizManager;
    }

    @Override
    public void startQuiz(List<Question> questions) {
        nextQuestion(questions);
        quizManagerView.updateTimer("");

    }

    @Override
    public void showResult(Context context, View view,int totalScore) {
        AlertDialog.Builder builder=new AlertDialog.Builder(context);

        final Button finish=view.findViewById(R.id.finishbtn);
        final TextView score=view.findViewById(R.id.txtTotalScore);

        score.setText("Your score is "+totalScore);

        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                quizManagerView.onQuizEnd();
            }
        });


        builder.setView(view);
        AlertDialog dialog=builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    void nextQuestion(List<Question> questions){

        for(int i=0;i<questions.size();i++){
            quizManagerView.nextQuestion(questions.get(i));
        }
    }
}
