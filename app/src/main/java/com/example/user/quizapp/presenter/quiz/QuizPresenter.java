package com.example.user.quizapp.presenter.quiz;

import android.content.Context;
import android.view.View;

import com.example.user.quizapp.model.Quiz.Question;

import java.util.List;

public interface QuizPresenter {
    void loadQuestions(Context context);

    interface QuizManger{
        void startQuiz(List<Question> questions);
        void showResult(Context context, View view,int totalScore);
    }

}
