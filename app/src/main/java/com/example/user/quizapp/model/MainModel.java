package com.example.user.quizapp.model;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.user.quizapp.classes.VolleySingleton;
import com.example.user.quizapp.presenter.MainPresenter;
import com.example.user.quizapp.view.MainView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainModel implements MainPresenter {

    MainView mainView;
    String categoryUrl="https://opentdb.com/api_category.php";
    QuizCategory quizCategory;
    List<QuizCategory> quizCategoryList=new ArrayList<>();

    public MainModel(MainView mainView) {
        this.mainView = mainView;
    }

    @Override
    public void getQuizCategory(final Context context) {

        //quizCategory=new QuizCategory();
        StringRequest stringRequest=new StringRequest(Request.Method.GET, categoryUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //Toast.makeText(context,response,Toast.LENGTH_SHORT).show();

                try {

                    JSONObject jsonObject=new JSONObject(response);
                    JSONArray jsonArray=jsonObject.getJSONArray("trivia_categories");


                    //Toast.makeText(context,jsonArray.length(),Toast.LENGTH_SHORT).show();

                    for(int i=0;i<jsonArray.length();i++){
                        JSONObject object =jsonArray.getJSONObject(i);
                        quizCategory=new QuizCategory();
                        quizCategory.cat_id=object.getInt("id");
                        quizCategory.cat_name=object.getString("name");
                        quizCategoryList.add(quizCategory);

                        //Toast.makeText(context,object.get("id").toString(),Toast.LENGTH_SHORT).show();

                    }



                } catch (JSONException e) {
                    Toast.makeText(context,e.toString(),Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
               mainView.afterLoadQuizCategory(quizCategoryList);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context,error.toString(),Toast.LENGTH_SHORT).show();
            }
        });

        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);

    }
}
