package com.example.user.quizapp.presenter;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.user.quizapp.R;
import com.example.user.quizapp.model.Constants;
import com.example.user.quizapp.view.MainView;

public class CustomDialog implements MainPresenter.CustomDialog  {

    MainView.CustomDialog customDialog;
    LayoutInflater layoutInflater;

    public CustomDialog(MainView.CustomDialog customDialog) {
        this.customDialog = customDialog;
    }

    @Override
    public void createDialog(Context context,View view) {

        AlertDialog.Builder builder=new AlertDialog.Builder(context);


        final Button easy=view.findViewById(R.id.easy);
        final Button medium=view.findViewById(R.id.medium);
        final Button hard=view.findViewById(R.id.hard);
        easy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customDialog.onSelectDialogOption(Constants.EASY);
            }
        });
        medium.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customDialog.onSelectDialogOption(Constants.MWDIUM);
            }
        });
        hard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customDialog.onSelectDialogOption(Constants.HARD);
            }
        });

        builder.setView(view);
        AlertDialog dialog=builder.create();
        dialog.show();

    }
}
