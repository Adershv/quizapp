package com.example.user.quizapp.model;

public class Constants {

    public final static String USER_INFO = "user_info" ;

    //login_status
    public static String LOGIN_STATUS = "login_status";
    public final static int LOGIN_IN = 1;
    public final static int LOGIN_OUT = 0;


    //Service
    public static String SERVICE = "service";
    public final static int FB_SERVICE = 3;
    public final static int GOOGLE_SERVICE = 4;

    //user's profile fields
    public static String USER_NAME = "user_name";
    public static String USER_EMAIL = "user_email";

    //Quiz Type
    public  static String EASY="easy";
    public  static String MWDIUM="medium";
    public static  String HARD="hard";

    public static String QUIZ_INFO="quiz_info";
    public  static String QUIZ_CATEGORY="quiz_category";
    public static  String QUIZ_TYPE="quiz_type";
}
