package com.example.user.quizapp.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.CountDownTimer;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.quizapp.R;
import com.example.user.quizapp.model.Constants;
import com.example.user.quizapp.model.Quiz.Question;
import com.example.user.quizapp.model.Quiz.QuizModel;
import com.example.user.quizapp.presenter.quiz.QizManager;
import com.example.user.quizapp.presenter.quiz.QuizPresenter;
import com.example.user.quizapp.view.quiz.QuizView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class QuizActivity extends AppCompatActivity implements QuizView, QuizView.QuizManager {


    private static final long START_TIME_IN_MILLIS = 120000;
    private long mTimeLeftInMillis = START_TIME_IN_MILLIS;

    QuizPresenter quizPresenter;
    QuizPresenter.QuizManger quizManager;

    Button next,start;
    TextView questionTxt,timerText,score,questionCount;
    RadioButton op1,op2,op3,op4;
    RadioGroup radioGroup;
    ConstraintLayout quizLayout;
    List<Question> questions=new ArrayList<>();

    CountDownTimer countDownTimer;

    Boolean isQuizStarted=false;

    int numOfQuestion;
    private static int currentQuestion=0;
    private static int mark=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        quizPresenter=new QuizModel(this);
        quizManager=new QizManager(this);

        quizPresenter.loadQuestions(this);

        radioGroup=findViewById(R.id.radioGroup);
        next=findViewById(R.id.next);
        start=findViewById(R.id.start);
        op1=findViewById(R.id.op1);
        op2=findViewById(R.id.op2);
        op3=findViewById(R.id.op3);
        op4=findViewById(R.id.op4);
        quizLayout=findViewById(R.id.quizlayout);
        questionTxt=findViewById(R.id.question);
        timerText=findViewById(R.id.timer);
        score=findViewById(R.id.score);
        questionCount=findViewById(R.id.questionCount);

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                isQuizStarted=!isQuizStarted;
                clearValues();
                //quizManager.startQuiz(questions);
                if(isQuizStarted){
                    start.setText("EXIT QUIZ");
                    startTimer();
                    quizLayout.setVisibility(View.VISIBLE);
                    if(currentQuestion<numOfQuestion) {
                        nextQuestion(questions.get(currentQuestion));
                        currentQuestion++;
                        updateQuestionNumber();
                    }
                    else {
                        Toast.makeText(QuizActivity.this,"Quiz over",Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    Intent intent=new Intent(QuizActivity.this,MainActivity.class);
                    startActivity(intent);
                    currentQuestion=0;
                    quizLayout.setVisibility(View.GONE);
                }


            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(currentQuestion<numOfQuestion) {
                    if(currentQuestion<numOfQuestion)
                        nextQuestion(questions.get(currentQuestion));
                        checkAnswer();
                        currentQuestion++;
                        updateQuestionNumber();
                }
                else {
                    View dialogView=getLayoutInflater().inflate(R.layout.show_result,null);
                    quizManager.showResult(QuizActivity.this,dialogView,mark);
                    Toast.makeText(QuizActivity.this,"Quiz over",Toast.LENGTH_SHORT).show();
                }
            }
        });




    }

    void updateQuestionNumber(){
        questionCount.setText("Questions : "+currentQuestion+""+"/"+""+numOfQuestion);
    }

    private void clearValues() {
        mark=0;
        currentQuestion=0;

    }

    private void checkAnswer() {
        if(radioGroup.getCheckedRadioButtonId()!=-1){
            int radioId=radioGroup.getCheckedRadioButtonId();
            RadioButton radioButton=findViewById(radioId);
            String selectedAns= (String) radioButton.getText();
            if(selectedAns.equals(questions.get(currentQuestion).getAnswer())){
                mark++;
            }
            radioGroup.clearCheck();
        }

        upadateScore(mark);
    }

    private void upadateScore(int mark) {
        score.setText("Score : "+mark+"");

    }

    private void startTimer() {
        countDownTimer=new CountDownTimer(mTimeLeftInMillis,1000) {
            @Override
            public void onTick(long l) {
                mTimeLeftInMillis=l;
                updateCountDownText();
            }

            @Override
            public void onFinish() {
                View dialogView=getLayoutInflater().inflate(R.layout.show_result,null);
                quizManager.showResult(QuizActivity.this,dialogView,mark);

            }
        }.start();
    }

    private void updateCountDownText() {
        int minutes = (int) (mTimeLeftInMillis / 1000) / 60;
        int seconds = (int) (mTimeLeftInMillis / 1000) % 60;

        String timeLeftFormatted = String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds);

        timerText.setText(timeLeftFormatted);
    }
    private void resetTimer() {
        mTimeLeftInMillis = START_TIME_IN_MILLIS;
        updateCountDownText();

    }

    @Override
    public void onQuestionLoadFinish(List<Question> questions) {

        this.questions=questions;
        numOfQuestion=questions.size();

    }


    public void nextQuestion(Question question) {

        List<String> list=new ArrayList<>();


        list.add(question.getOp1());
        list.add(question.getOp2());
        list.add(question.getOp3());
        list.add(question.getAnswer());

        Collections.shuffle(list);



        questionTxt.setText(currentQuestion+1+" "+question.getQuestion());
        op1.setText(list.get(0));
        op2.setText(list.get(1));
        op3.setText(list.get(2));
        op4.setText(list.get(3));


    }

    @Override
    public void updateTimer(String s) {

    }

    @Override
    public void onQuizEnd() {

        Intent intent=new Intent(QuizActivity.this,MainActivity.class);
        startActivity(intent);
        finish();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        countDownTimer.cancel();
    }
}
